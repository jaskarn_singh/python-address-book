#!/usr/bin/python3
# Jaskarn Singh 1313787
import re
import csv
read1 = open("addbookdata.csv", "a")

# method to add a contact
def add_data():
	#Check Input against key parameters so input fits requirements
	def getlname(message):
		while True:
			chlname = input(message)
			x = re.fullmatch("[a-zA-Z]+", chlname)
			#print (x)

			if x == None:
				print("You must enter letters for a Last Name ")
			else:
				#print("Valid Last Name ")
				return chlname
				
	
	def getfname(message):
		while True:
			chfname = input(message)
			x = re.fullmatch("[a-zA-Z]+", chfname)
			#print (x)

			if x == None:
				print("You must enter letters for a First Name ")
			else:
				#print("Valid First Name ")
				return chfname

	def getstreetName(message):
		while True:
			chstreetName = input(message)
			x = re.fullmatch("[a-zA-Z ]+", chstreetName)
			#print (x)

			if x == None:
				print("You must enter Relevant Street Name ")
			else:
				#print("Valid Street Name ")
				return chstreetName

	def getstreetNum(message):
		while True:
			chstreetNum = input(message)
			x = re.fullmatch("[0-9]+", chstreetNum)
			#print (x)

			if x == None:
				print("You must enter Relevant Street Number ")
			else:
				#print("Valid Street Number ")
				return chstreetNum
	
	def getcity(message):
		while True:
			chcity = input(message)
			x = re.fullmatch("[a-zA-Z ]+", chcity)
			#print (x)

			if x == None:
				print("You must enter Relevant Street Name ")
			else:
				#print("Valid City Name")
				return chcity

	def getphone(message):
		while True:
			chphone = input(message)
			x = re.fullmatch("[0-9 ]+", chphone)
			#print (x)

			if x == None:
				print("You must enter Relevant Phone Number ")
			else:
				#print("Valid Phone Number ")
				return chphone
	
	def getemail(message):
		while True:
			chemail = input(message)
			x = re.fullmatch("[^@]+@[^@]+\.[^@]+", chemail)
			#print (x)

			if x == None:
				print("You must enter Relevant Email")
			else:
				#print("Valid Email ")
				return chemail

	def getuclass(message):
		while True:
			chuclass = input(message)
			x = re.fullmatch("[a-zA-Z0-9 ]+", chuclass)
			#print (x)

			if x == None:
				print("You must enter Relevant Class Information ")
			else:
				#print("Valid Class Information ")
				return chuclass

	def getnotes(message):
		while True:
			chnotes = input(message)
			x = re.fullmatch("[a-zA-Z0-9 ]+", chnotes)
			#print (x)

			if x == None:
				print("You must enter Relevant Notes ")
			else:
				#print("Valid Notes ")
				return chnotes
	
	#User Input for all information to be added
	print("-----------------------------------------------------")
	print("Please type in the information about the new contact ")
	print("-----------------------------------------------------")
	print("                                                     ")
	lname = getlname("What is contact's Last Name? ")
	fname = getfname("What is contact's First Name? ")
	print("-------------------------------------------------------------------------")
	print("Please enter the correct information for the address in the right fields ")
	print("-------------------------------------------------------------------------")
	print("                                                                         ")
	streetName = getstreetName("Street Name of the contacts address? ")
	streetNum = getstreetNum("Street Number of the contacts address? ")
	city = getcity("City of contacts address? ")
	phone = getphone("What is the contact's phone number? ")
	email = getemail("What is contact's email? ")
	uclass = getuclass("What classes they share with you? ")
	notes = getnotes("Any additional notes about contact? ")
	print("---------------------------------------------------------")
	print("The following is the information we have for the contact ")
	print("---------------------------------------------------------")
	print("                                                         ")
	#An Array displaying all information being added
	contact = [lname, fname,\
			streetName, streetNum, city,\
			phone,email,\
			uclass, notes]
	print(contact)

	# Writes all contact details to be added to the text file/address book
	read1 = open("addbookdata.csv", "a")
	read1.write(fname + ', ' + lname + ', ' + streetName + ', ' + streetNum + ', ' + city + ', ' + phone + ', ' + email + ', ' + uclass + ', ' + notes)
	read1.write("\n")
	print("The contact has been added")
	read1.close()
	cont()

#method to search for people in the text file/address book
def search_data():
	print("--------------------------------------------------------------------------------")
	uinput1 = input("Please enter any keywords, including address, email, phone or name of a existing contact in the address book: ")
	print("--------------------------------------------------------------------------------")
	print("                                                                                ")
	#Reads text file/address book for existing contact prints all lines that match keyword
	read1 = open("addbookdata.csv", "r")
	for a in read1:
		if uinput1 in a:
			print(a)
		else:
			continue
			#print("This contact does not exist, Please add this contact or check spelling")
	cont()

#method to list the information in a default list (read)
def def_list():
		print("------------------------------------------------------------")
		print("| Below is all the contacts contained in the address book: |")
		print("------------------------------------------------------------")
		print("                                                            ")
		#reads line of file one by one displaying all contact info per contact
		read1 = open("addbookdata.csv", "r")
		readfile = read1.read()
		print (readfile)
		cont()

def fname_list():
	cont()
	"""def fn(e):
		return e['fname']

	read1 = open("addbookdata.csv", "r")
	readfile = read1.readlines()
	for r in readfile:
		#arr = r.split(",")
	#y = sort(readfile[0])
	#print (y)
		readfile.sort(key=fn)
		print(readfile)"""
	
def lname_list():
	cont()
	"""from operator  import itemgetter, attrgetter
	read1 = open("addbookdata.csv", "r")
	readfile = read1.readlines()
	#print (readfile)
	
	for r in readfile:
		arr = r.split(",")
		#print (arr)
	#y = sorted(readfile, key= lambda r : readfile[1])
		y = sorted(arr, key=itemgetter(0))
		#for a in readfile:
		print (y)"""
def class_list():
	cont()

def city_list():
	cont()

#method lists all the contacts in the text file/address book
def list_contacts():
	
	print("---------------------------------------")
	print("|        Address Book Viewer          |")
	print("---------------------------------------")
	print("|          Default List [d]           |")
	print("|        List by First Name [f]       |")
	print("|        List by Last Name [q]        |")
	print("|          List by Class [c]          |")
	print("|           List by City [t]          |")
	print("|           Back to Main [n]          |")
	print("---------------------------------------")
	userinput4 = input()
	
	if userinput4 == 'd':
		print("You have chosen to View Default List")
		def_list()
	elif userinput4 == 'f':
		print("You have chosen to View List sorted by First Name")
		fname_list()
	elif userinput4 == 'q':
		print("You have chosen to View List sorted by Last Name")
		lname_list()
	elif userinput4 == 'c':
		print("You have chosen to View List sorted by Class")
		class_list()
	elif userinput4 == 't':
		print("You have chosen to View List sorted by City")
		city_list()
	elif userinput4 == 'n':
		print("You have chosen to Return back to Main Window")
		main()
	else:
		print("Not a valid command, Program will restart")
		list_contacts()


# method deletes a contact from the text file/address book

def del_contact():
	#Asks for user input of who to delete
	import fileinput
	print("--------------------------------------------------------------------------")
	delcon = input("Please enter the name of a existing contact in the address book: ")
	print("--------------------------------------------------------------------------")
	print("                                                                          ")
	#based off user input identifies existing contact which is then deleted
	print("Contact: ", delcon, "is being deleted")
	for a in fileinput.input("addbookdata.csv", inplace =1):
		a = a.strip()
		if not delcon in a:
			print(a)
	print("The contact has been deleted from the address book ")
	read1.close()
	cont()

# method to edit contact details
def edit_contact():
	#searches for a contact by name off user input
	print("---------------------------------------------------------------")
	ename = input("Please enter the name of the contact you wish to edit? ")
	print("---------------------------------------------------------------")
	print("                                                               ")
	#Looks for the contact given and then removes their information
	read1 = open("addbookdata.csv", "r")
	for a in read1:
		if ename in a:
			print(a)
			import fileinput
			for a in fileinput.input("addbookdata.csv", inplace =1):
				a = a.strip()
				if not ename in a:
					print(a)
			ed_data()
		else:
			continue
			#print("Could not find contact ")

#lastly the informtion is readded in for the contact completing the edit (Similar to add function)
def ed_data():
	#Check Input against key parameters so input fits requirements
	def getlname(message):
		while True:
			chlname = input(message)
			x = re.fullmatch("[a-zA-Z]+", chlname)
			#print (x)

			if x == None:
				print("You must enter letters for a Last Name ")
			else:
				#print("Valid Last Name ")
				return chlname
				
	
	def getfname(message):
		while True:
			chfname = input(message)
			x = re.fullmatch("[a-zA-Z]+", chfname)
			#print (x)

			if x == None:
				print("You must enter letters for a First Name ")
			else:
				#print("Valid First Name ")
				return chfname

	def getstreetName(message):
		while True:
			chstreetName = input(message)
			x = re.fullmatch("[a-zA-Z ]+", chstreetName)
			#print (x)

			if x == None:
				print("You must enter Relevant Street Name ")
			else:
				#print("Valid Street Name ")
				return chstreetName

	def getstreetNum(message):
		while True:
			chstreetNum = input(message)
			x = re.fullmatch("[0-9]+", chstreetNum)
			#print (x)

			if x == None:
				print("You must enter Relevant Street Number ")
			else:
				#print("Valid Street Number ")
				return chstreetNum
	
	def getcity(message):
		while True:
			chcity = input(message)
			x = re.fullmatch("[a-zA-Z ]+", chcity)
			#print (x)

			if x == None:
				print("You must enter Relevant Street Name ")
			else:
				#print("Valid City Name")
				return chcity

	def getphone(message):
		while True:
			chphone = input(message)
			x = re.fullmatch("[0-9 ]+", chphone)
			#print (x)

			if x == None:
				print("You must enter Relevant Phone Number ")
			else:
				#print("Valid Phone Number ")
				return chphone
	
	def getemail(message):
		while True:
			chemail = input(message)
			x = re.fullmatch("[^@]+@[^@]+\.[^@]+", chemail)
			#print (x)

			if x == None:
				print("You must enter Relevant Email")
			else:
				#print("Valid Email ")
				return chemail

	def getuclass(message):
		while True:
			chuclass = input(message)
			x = re.fullmatch("[a-zA-Z0-9 ]+", chuclass)
			#print (x)

			if x == None:
				print("You must enter Relevant Class Information ")
			else:
				#print("Valid Class Information ")
				return chuclass

	def getnotes(message):
		while True:
			chnotes = input(message)
			x = re.fullmatch("[a-zA-Z0-9 ]+", chnotes)
			#print (x)

			if x == None:
				print("You must enter Relevant Notes ")
			else:
				#print("Valid Notes ")
				return chnotes
	print("---------------------------------------------------------")
	print("Please type in the updated information about the contact ")
	print("---------------------------------------------------------")
	print("                                                         ")
	lname = getlname("What is contact's Last Name? ")
	fname = getfname("What is contact's First Name? ")
	print("-------------------------------------------------------------------------")
	print("Please enter the correct information for the address in the right fields ")
	print("-------------------------------------------------------------------------")
	print("                                                                         ")
	streetName = getstreetName("Street Name of the contacts address? ")
	streetNum = getstreetNum("Street Number of the contacts address? ")
	city = getcity("City of contacts address? ")
	phone = getphone("What is the contact's phone number? ")
	email = getemail("What is contact's email? ")
	uclass = getuclass("What classes they share with you? ")
	notes = getnotes("Any additional notes about contact? ")
	print("---------------------------------------------------------")
	print("The following is the information we have for the contact ")
	print("---------------------------------------------------------")
	print("                                                         ")
	contact = [lname, fname,\
			streetName, streetNum, city,\
			phone, email, \
			uclass, notes]
	print(contact)

	# Writes all contact details to the text file/address book
	read1 = open("addbookdata.csv", "a")
	read1.write(fname + ', ' + lname + ', ' + streetName + ', ' + streetNum + ', ' + city + ', ' + phone + ', ' + email + ', ' + uclass + ', ' + notes)
	read1.write("\n")
	print("The contact has been updated")
	read1.close()
	cont()

#main method for the address book (initial display for functionality)
def main():
	print("---------------------------------------")
	print("Welcome to the University Address Book ")
	print("---------------------------------------")
	#print("In this address book you will be able to add, delete, edit, search and view all contacts ")
	print("|     All information is stored       |")
	print("---------------------------------------")
	print("|      Please select a option         |")
	print("---------------------------------------")
	print("|       Add contact press [1]         |")
	print("|      Delete contact press [2]       |")
	print("|       Edit contact press [3]        |")
	print("|   Search for a contact press [4]    |")
	print("|     View all contacts press [5]     |")
	print("|           End Program [n]           |")
	print("---------------------------------------")
	userinput = input()
	
	#conditionals for navigation based on user input
	if userinput == '1':
		print("You have chosen to add a contact")
		add_data()
	elif userinput == '2':
		print("You have chosen to delete a contact")
		del_contact()
	elif userinput == '3':
		print("You have chosen to edit a contact")
		edit_contact()
	elif userinput == '4':
		print("You have chosen to search for a contact")
		search_data()
	elif userinput == '5':
		print("You have chosen to view all contacts")
		list_contacts()
	elif userinput == 'n':
		print("_")
	else:
		print("Not a valid command, Program will restart")
		main()
#method to reload the address book to keep the program flowing
def cont():
	uinput2 = input("Would you like to restart the program to select another option? [y/n]")
	if uinput2 == 'y':
		main()
	elif uinput2 == 'n':
		print("_")
	else:
		print("Not a valid command")
		cont()
main()
	
